exports.config = {
    framework: 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: [
        './specs/task2.1-spec.js',
        './specs/task2.2-spec.js',
        './specs/task2.3-spec.js'
    ],
    multiCapabilities: [{
        browserName: 'chrome',
        acceptInsecureCerts: true
    }]
}

