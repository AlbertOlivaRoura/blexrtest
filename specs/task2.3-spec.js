describe('The bookie selector ', function () {

    beforeEach(function () {
        browser.ignoreSynchronization = true;
        browser.get('https://www.sbo.net/');
    });

    it('shows modal', function () {
        element(by.css('[class="kgw-header for-picker"]')).click();
        expect(element(by.css('[class="wizard-body"]')).isDisplayed()).toBe(true);
    });

    it('check Desktop, 50$-200$, yes, Claim link', async function () {
        let windowID = browser.getWindowHandle();

        element(by.css('[class="kgw-header for-picker"]')).click(); //display

        do {
            element.all(by.css('[class="back-link choose-step"]')).each(function (element, index) {
                element.getText().then(function (text) {
                    if (text == 'Start again') {
                        element.click();
                    }
                });
            });
            element.all(by.css('[data-step="2"]')).each(function (element, index) {
                element.getText().then(function (text) {
                    if (text == 'Desktop') {
                        element.click();
                    }
                });
            });

            element.all(by.css('[data-step="3"]')).each(function (element, index) {
                element.getText().then(function (text) {
                    if (text == '€50 - €200') {
                        element.click();
                    }
                });
            });

            element.all(by.css('[data-step="4"]')).each(function (element, index) {
                element.getText().then(function (text) {
                    if (text == 'YES') {
                        element.click();
                    }
                });
            });

            browser.sleep(1000);

            var displayed = await element(by.css('[class="claim-button"]')).isDisplayed().then(disp => {
                return disp;
            });

        } while (!displayed);

        element(by.css('[class="claim-button"]')).click();
        browser.sleep(2000);

        await browser.getWindowHandle().then(function (parentGUID) {
            browser.getAllWindowHandles().then(function (allGUID) {
                for (let guid of allGUID) {
                    if (guid != parentGUID) {
                        browser.switchTo().window(guid);
                        expect(browser.getCurrentUrl()).toBe('https://lp.22betpartners.com/p/sports-general/index_en.php?tag=d_88427m_8693c_SBO_Review');
                    }
                }
            });
        });
    });
});