describe('Test links table', function () {

    beforeEach(function () {
        browser.ignoreSynchronization = true;
        browser.get('https://www.vegasslotsonline.com/real-money');
    });

    it('live casino /live-dealer/', function () {
        element(by.linkText('live casino')).click();
        expect(browser.getCurrentUrl()).toContain('/live-dealer/');
    });

    it('payment methods /deposit-methods/', function () {
        element(by.linkText('payment methods')).click();
        expect(browser.getCurrentUrl()).toContain('/deposit-methods/');
    });

    it('all gambling games /table-games/', function () {
        element(by.linkText('all gambling games')).click();
        expect(browser.getCurrentUrl()).toContain('/table-games/');
    });

    it('new games /new-online-slots/', function () {
        element(by.linkText('new games')).click();
        expect(browser.getCurrentUrl()).toContain('/new-online-slots/');
    });

    it('all software providers you’ll find at online casinos /software-providers/', function () {
        element(by.linkText('all software providers you’ll find at online casinos')).click();
        expect(browser.getCurrentUrl()).toContain('software-providers');
    });

});