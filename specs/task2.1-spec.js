describe('Test bottom links list', function () {

  beforeEach(function () {
    browser.ignoreSynchronization = true;
    browser.get('https://www.vegasslotsonline.com/');
  });

  it('Contact us', function () {
    element(by.linkText('Contact Us')).click();
    expect(browser.getTitle()).toEqual('Contact Us - Vegas Slots Online');
  });

  it('Responsible Gambling', function () {
    element(by.linkText('Responsible Gambling')).click();
    expect(browser.getTitle()).toEqual('Responsible Gambling');
  });

  it('Fairness and Testing', function () {
    element(by.linkText('Fairness and Testing')).click();
    expect(browser.getTitle()).toEqual('Gaming Fairness Labs – Online Casino Testing');
  });

  it('Licensing Bodies and Regulators', function () {
    element(by.linkText('Licensing Bodies and Regulators')).click();
    expect(browser.getTitle()).toEqual('Online Gambling Regulators and Licensing Bodies');
  });

  it('Sitemap', function () {
    element(by.linkText('Sitemap')).click();
    expect(browser.getTitle()).toEqual('Sitemap');
  });

  it('Privacy Policy', function () {
    element(by.linkText('Privacy Policy')).click();
    expect(browser.getTitle()).toEqual('Privacy Policy page');
  });
});
