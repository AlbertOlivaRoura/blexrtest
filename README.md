# Blexr UI Tests

I have made a 1 minute video so that in case you have any problems with the execution of the tests, you can see how they run on my machine.

https://photos.app.goo.gl/PxTETDnucQhKA6Xc7

## Run Tests

With a clone of the code start by installing dependencies:

```
npm install
```

Then update _Selenium_ drivers:

```
npm run update-drivers
```

Lastly, run the tests:

- `protractor conf.js`

## Architecture

The different building blocks of the framework are:

- **specs**: Test files

### Specs

Tests should be **completely independent** and the order of execution should never be relied on.
Prefer to separate tests in different specs than create complex and hard to understand preconditions.

Minimize the number of assertions in each test.
Ideally tests have only one assertion, but may have more if absolutely necessary.
Assertions (matchers) should be clear and easy to understand.
Keep them short!

---

**Task 2.1**

I have created a test for each link that verifies that the title of the page is the one that corresponds in each case

---

**Task 2.2**

I have created a test for each link that verifies that the url of the loaded page is the one that corresponds in each case

The links "all gambling", "new games" and "all software providers you will find in online casinos" are missing so these tests will fail.

---

**Task 2.3**

The objective was to prove that the link of the "Claim" button is not broken, but I have also added another test to ensure that the modal that contains that button does not fail.

Should the modal load fail, the test that checks the button link would fail, which might not be true. Since the link could continue to work but we could not access it. In this way, having the 2 tests, if the modal load fails, we will see it in the test that checks that load and it will give us much more accurate results

I have seen that the "Claim" button does not always appear, so I have implemented a loop so that it tries to get to the button and that the test does not continue until it succeeds.

---